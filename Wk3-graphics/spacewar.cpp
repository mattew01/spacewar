

#include "spacewar.h"

Spacewar::Spacewar()
{}

Spacewar::~Spacewar()
{
	releaseAll();
}

// initialize the game
void Spacewar::initialize( HWND hwnd )
{
	Game::initialize( hwnd ); // call game's initialize
	// initialize the textures
	if( !bgTexture.initialize( graphics, SPACE_IMAGE ) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR, 
			"Error initializing space texture" ) );
	}
	if( !gameTextures.initialize( graphics, TEXTURES_IMAGE ) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR, 
			"Error initializing game textures" ) );
	}

	// initialize the Images
	if( !space.initialize( graphics, 0, 0, 0, &bgTexture ) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR, 
			"Error initializing space image" ) );
	}
	space.setScale( SPACE_SCALE );
	//if( !planet.initialize( this, planetNS::WIDTH, planetNS::HEIGHT, planetNS::TEXTURE_COLS, &gameTextures ) )
	//{
	//	throw( GameError(gameErrorNS::FATAL_ERROR, 
	//		"Error initializing planet" ) );
	//}
	//// place the planet in the center of the screen 
	//planet.setX( GAME_WIDTH*0.5f - planet.getWidth()*0.5f ); 
	//planet.setY( GAME_HEIGHT*0.5f - planet.getHeight()*0.5f );

	if( !ship1.initialize( this, shipNS::WIDTH, shipNS::HEIGHT, shipNS::TEXTURE_COLS, &gameTextures ) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR, 
			"Error initializing ship 1" ) );
	}
	// starting screen position for the ship
	ship1.setX( GAME_WIDTH/2 - shipNS::WIDTH/2 );
	ship1.setY( GAME_HEIGHT/2 - shipNS::HEIGHT/2);

	// turns on the animation (lets Image know this image is animated)
	ship1.setFrames( shipNS::SHIP1_START_FRAME, shipNS::SHIP1_END_FRAME );  // set up the animation frames
	ship1.setCurrentFrame( shipNS::SHIP1_START_FRAME ); // starting frame
	ship1.setFrameDelay( shipNS::SHIP_ANIMATION_DELAY ); // how long to show each frame

	//ship1.setVelocity( VECTOR2( shipNS::SPEED, -shipNS::SPEED) );


	// SHIP 2
	//if( !ship2.initialize( this, shipNS::WIDTH, shipNS::HEIGHT, shipNS::TEXTURE_COLS, &gameTextures ) )
	//{
	//	throw( GameError(gameErrorNS::FATAL_ERROR, 
	//		"Error initializing ship 2" ) );
	//}
	// starting screen position for the ship
	//ship2.setX( GAME_WIDTH*3/4 );
	//ship2.setY( GAME_HEIGHT*3/4 );

	// turns on the animation (lets Image know this image is animated)
	//ship2.setFrames( shipNS::SHIP2_START_FRAME, shipNS::SHIP2_END_FRAME );  // set up the animation frames
	//ship2.setCurrentFrame( shipNS::SHIP2_START_FRAME ); // starting frame
	//ship2.setFrameDelay( shipNS::SHIP_ANIMATION_DELAY ); // how long to show each frame

	//ship2.setVelocity( VECTOR2( -shipNS::SPEED, -shipNS::SPEED) ) ;

	return;
}

void Spacewar::update()
{  
    
    if (ship1.getActive())
    {
        if (input->isKeyDown(SHIP_UP_KEY))   // if engine on
        {
            ship1.setEngineOn(true);
                
        }
        else
        {
            ship1.setEngineOn(false);
         
        }
        ship1.rotate(shipNS::NONE);
        if (input->isKeyDown(SHIP_LEFT_KEY) )   // if turn ship1 left
            ship1.rotate(shipNS::LEFT);
        if (input->isKeyDown(SHIP_RIGHT_KEY)) // if turn ship1 right
            ship1.rotate(shipNS::RIGHT);
    }
    

	// -- 9.1.2 Moving the Background -------------
	// When the ship moves, we move the location of the space image in the opposite direction

    // Update the entities
    ship1.update(frameTime);
    // move space in X direction opposite ship
	
    space.setX(space.getX() - frameTime * ship1.getVelocity().x);
    // move space in Y direction opposite ship
    space.setY(space.getY() - frameTime * ship1.getVelocity().y);

    // Wrap space image around at edge
    // if left edge of space > screen left edge
    if (space.getX() > 0)               
        // move space image left by SPACE_WIDTH
        space.setX(space.getX() - SPACE_WIDTH);
    // if space image off screen left
    if (space.getX() < -SPACE_WIDTH)
        // move space image right by SPACE_WIDTH
        space.setX(space.getX() + SPACE_WIDTH);
    // if top edge of space > screen top edge
    if (space.getY() > 0)
        // move space image up by SPACE_HEIGHT
        space.setY(space.getY() - SPACE_HEIGHT);
    // if space image off screen top
    if (space.getY() < -SPACE_HEIGHT)
        // move space image down by SPACE_IMAGE
        space.setY(space.getY() + SPACE_HEIGHT);

	//we move it until its completely offscreen, then we move it back into screen

	// eventually, the space image will move far enough so it does no cover the entire screen
	// when this happens, we will draw the space image, move it, and draw it again until the entire screen is covered.
	// drawing is in the render function!
	
	
	
	// makes the ship attracted to the planet
	//ship1.gravityForce( &planet, frameTime );
	//ship2.gravityForce( &planet, frameTime );

	//animate our ship
	//ship1.update(frameTime);
	//ship2.update(frameTime);
	//planet.update(frameTime);
	/*
	// rotate
	ship.setDegrees( ship.getDegrees() + frameTime * 180.0f );

	// scale
	ship.setScale( ship.getScale() - frameTime * 0.2f );

	// move
	ship.setX( ship.getX() + frameTime * 100.0f );

	if( ship.getX() > GAME_WIDTH ) // if offscreen right
	{
		ship.setX( (float)-ship.getWidth() ); // position offscreen left
		ship.setScale( 1.5f ); // reset the scale to 1.5
	}
	

	if( input->isKeyDown( SHIP_RIGHT_KEY ) || 
		input->isKeyDown( D_KEY ))
	{
		if( ship.getX() < (GAME_WIDTH - ship.getWidth()) )
		{
			ship.setX( ship.getX() + frameTime * SHIP_SPEED );
		}
	}
	if( input->isKeyDown( SHIP_LEFT_KEY ) )
	{
		if( ship.getX() > 0 ){
			ship.setX( ship.getX() - frameTime * SHIP_SPEED );
		}
	}
	if( input->isKeyDown( SHIP_UP_KEY ) )
	{
		ship.setY( ship.getY() - frameTime * SHIP_SPEED );
	}
	if( input->isKeyDown( SHIP_DOWN_KEY ) )
	{
		ship.setY( ship.getY() + frameTime * SHIP_SPEED );
	}

	*/

	//space.setX( space.getX() - frameTime * ship1.getVelocity().x );
	//space.setY( space.getY() - frameTime * ship1.getVelocity().y );

	//if( space.getX() > 0 )
	//	space.setX( space.getX() - SPACE_WIDTH );
	//if( space.getX() < -SPACE_WIDTH )
	//	space.setX( space.getX() + SPACE_WIDTH );
	//if( space.getY() > 0 )
	//	space.setY( space.getY() - SPACE_HEIGHT );
	//if( space.getY() < -SPACE_HEIGHT )
	//	space.setY( space.getY() + SPACE_HEIGHT );
}

void Spacewar::ai()
{ }

void Spacewar::collisions()
{ 
	/*
	VECTOR2 collisionVector;
	if( ship1.collidesWith(planet, collisionVector ) )
	{
		// bounce off the planet
		ship1.bounce( collisionVector, planet );
		ship1.damage( PLANET );
	}
	if( ship2.collidesWith(planet, collisionVector ) )
	{
		// bounce off the planet
		ship2.bounce( collisionVector, planet );
		ship2.damage( PLANET );
	}
	if( ship1.collidesWith( ship2, collisionVector ) )
	{
		ship1.bounce( collisionVector, ship2 );
		ship1.damage( SHIP );

		ship2.bounce( collisionVector * -1, ship1 ); // change the direction of the collisionVector
		ship2.damage( SHIP );
	}
	*/
}

void Spacewar::render()
{ 
	float x = space.getX();
	float y = space.getY();

	graphics->spriteBegin();
	// call draw functions after!  ORDER IS IMPORTANT!
	
	space.draw();

	if( space.getX() < -SPACE_WIDTH + (int)GAME_WIDTH ) {
		space.setX(space.getX() + SPACE_WIDTH);
		space.draw();
	}
	if( space.getY() < -SPACE_HEIGHT + (int)GAME_HEIGHT ) {
		space.setY(space.getY() + SPACE_HEIGHT);
		space.draw();
		space.setX(x); // reset x

		if( space.getX() < -SPACE_WIDTH + (int)GAME_WIDTH ) {
			space.draw();
		}
	}
	space.setY(y); // reset y
	
	//planet.draw();
	ship1.draw();
	//ship2.draw();
	
	// call draw functions before!
	graphics->spriteEnd();
}

void Spacewar::releaseAll()
{
	gameTextures.onLostDevice();
	bgTexture.onLostDevice();
	Game::releaseAll();
}

void Spacewar::resetAll()
{
	bgTexture.onResetDevice();
	gameTextures.onResetDevice();
	Game::resetAll();
}
