// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 3 constants.h v1.0

#ifndef _CONSTANTS_H            // prevent multiple definitions if this 
#define _CONSTANTS_H            // ..file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include <windows.h>

//-----------------------------------------------
// Useful macros
//-----------------------------------------------
// Safely delete pointer referenced item
#define SAFE_DELETE(ptr)       { if (ptr) { delete (ptr); (ptr)=NULL; } }
// Safely release pointer referenced item
#define SAFE_RELEASE(ptr)      { if(ptr) { (ptr)->Release(); (ptr)=NULL; } }
// Safely delete pointer referenced array
#define SAFE_DELETE_ARRAY(ptr) { if(ptr) { delete [](ptr); (ptr)=NULL; } }
// Safely call onLostDevice
#define SAFE_ON_LOST_DEVICE(ptr) { if(ptr) { ptr->onLostDevice(); } }
// safely call onResetDevice
#define SAFE_ON_RESET_DEVICE(ptr) { if(ptr) { ptr->onResetDevice(); } }
// transparency color (the color that's considered to be transparent)
#define TRANSCOLOR SETCOLOR_ARGB( 0,255,0,255 ) // magenta

//-----------------------------------------------
//                  Constants	
//-----------------------------------------------

// window
const char CLASS_NAME[] = "Spacewar";
const char GAME_TITLE[] = "SpaceWar";
const bool FULLSCREEN = false;              // windowed or fullscreen
const UINT  GAME_WIDTH =  640;              // width of game in pixels
const UINT  GAME_HEIGHT = 480;              // height of game in pixels

// game
const double PI = 3.14159265;
const float FRAME_RATE = 200.0f;		// the target frame rate
const float MIN_FRAME_RATE = 10.0f;		// minimum frame rate
const float MIN_FRAME_TIME = 1.0f/FRAME_RATE; // minimum desired time for 1 frame
const float MAX_FRAME_TIME = 1.0f/MIN_FRAME_RATE; // maximum time

// key mappings
// In this game simple constants are used for key mappings. If variables were used
// it would be possible to save and restore key mappings from a data file.
const UCHAR CONSOLE_KEY		= VK_OEM_3;			// ~ for US keyboards
const UCHAR ESC_KEY			= VK_ESCAPE;        // escape key
const UCHAR SHIP_LEFT_KEY	= VK_LEFT;		
const UCHAR SHIP_RIGHT_KEY	= VK_RIGHT;
const UCHAR SHIP_UP_KEY		= VK_UP;
const UCHAR SHIP_DOWN_KEY	= VK_DOWN;
const UCHAR Q_KEY			= 0x51;
const UCHAR E_KEY			= 0x45;
const UCHAR	A_KEY			= 0x41;
const UCHAR S_KEY			= 0x53;
const UCHAR D_KEY			= 0x44;
const UCHAR W_KEY			= 0x57;

// graphic images
const char NEBULA_IMAGE[] = "pictures\\orion.jpg";
//const char PLANET_IMAGE[] = "pictures\\planet.png";
//const char SHIP_IMAGE[] = "pictures\\ship.png";
const char TEXTURES_IMAGE[] = "pictures\\textures.png";
const char FONT_IMAGE[] = "pictures\\CKfont.png";

const char SPACE_IMAGE[] = "pictures\\space.jpg";
const int SPACE_SCALE = 2;
const int SPACE_WIDTH = 640 * SPACE_SCALE;
const int SPACE_HEIGHT = 480 * SPACE_SCALE;

// weapon types
enum WEAPON { TORPEDO, SHIP, PLANET };


#endif
